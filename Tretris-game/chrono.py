from pygame.time import get_ticks

class Chrono:
    def __init__ (self, duree, repetition = False, fonction = None):
        self.repetition = repetition
        self.fonction = fonction
        self.duree = duree
        
        self.temps_depart = 0
        self.actif = False
        
    def activation(self):
        self.actif = True
        self.temps_depart = get_ticks()
        
    def desactivation (self):
        self.actif = False
        self.temps_depart = 0
        
    def update(self):
        temps_actuel = get_ticks()
        if temps_actuel - self.temps_depart >= self.duree and self.actif:
            
            #appelle une fonction
            if self.fonction and self.temps_depart != 0:
                self.fonction()
            
            #reset chrono
            self.desactivation()
            
            #repetion du chrono
            if self.repetition:
                self.activation()
from parametres import *
from sys import exit
from random import *

# composants
from jeu import Jeu
from score import Score
from aperçu import Aperçu

class Principal:
    def __init__(self):

        # general 
        pygame.init()
        pygame.mixer.init()
        self.display_surface = pygame.display.set_mode((FENETRE_LARGEUR,FENETRE_HAUTEUR))
        self.clock = pygame.time.Clock()
        pygame.display.set_caption('Tetris')
        pygame.mixer.music.load(choice(["MUSIC.mp3",]))
        pygame.mixer.music.play()

        # composants
        self.jeu = Jeu()
        self.score = Score()
        self.aperçu = Aperçu()

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    exit()

            # surface 
            self.display_surface.fill(GRIS)
            
            # commposants
            self.jeu.run()
            self.score.run()
            self.aperçu.run()

            # mettre a jour le jeu
            pygame.display.update()
            self.clock.tick()

if __name__ == '__main__':
    principal = Principal()
    principal.run()
from parametres import *
from random import choice

from chrono import Chrono
class Jeu:
    def __init__(self):

        # general 
        self.surface = pygame.Surface((LARGEUR_JEU, HAUTEUR_JEU))
        self.display_surface = pygame.display.get_surface()
        self.rect = self.surface.get_rect(topleft = (ECART, ECART))
        self.sprites = pygame.sprite.Group()
    
        # lignes
        self.line_surface = self.surface.copy()
        self.line_surface.fill((0,255,0))
        self.line_surface.set_colorkey((0,255,0))
        self.line_surface.set_alpha(150)
        
        #tetromino
        self.tetromino = Tetromino(choice(list(TETROMINOS.keys())),self.sprites)
        
        #chrono
        self.chronos = {
            "deplacement vertical" : Chrono(VITESSE_MAJ_JEU, True, self.bouger_bas),
            "deplacement horizontal" : Chrono(TEMPS_ATTENTE_DEPLACEMENT)
        }
        self.chronos["deplacement vertical"].activation()
        
    def chrono_update(self):
        for chrono in self.chronos.values():
            chrono.update()
    
    def bouger_bas(self):
        self.tetromino.bouger_bas()
    
    def bouger_haut(self):
        self.tetromino.bouger_haut()
    
    
    def quadrillage(self):
        
        for col in range(1,COLONNES):
            x = col * TAILLE_CARREAU
            pygame.draw.line(self.line_surface, COULEUR_LIGNE, (x,0), (x,self.surface.get_height()), 1)
            
        for ligne in range(1,RANGEES):
            y = ligne * TAILLE_CARREAU
            pygame.draw.line(self.line_surface, COULEUR_LIGNE, (0,y), (self.surface.get_width(),y), 1)
    
        self.surface.blit(self.line_surface, (0,0))
       
    def bouger (self):
        clés = pygame.key.get_pressed()
        if not self.chronos["deplacement horizontal"].actif:
            if clés[pygame.K_LEFT]:
                self.tetromino.bouger_horizontal(-1)
                self.chronos["deplacement horizontal"].activation()
            if clés[pygame.K_RIGHT]:
                self.tetromino.bouger_horizontal(1)
                self.chronos["deplacement horizontal"].activation()
            if clés[pygame.K_DOWN]:
                self.tetromino.bouger_bas()
                self.chronos["deplacement horizontal"].activation()
            if clés[pygame.K_UP]:
                self.tetromino.bouger_haut()
                self.chronos["deplacement horizontal"].activation()
        
    def run(self):
        
        #update
        self.bouger()
        self.chrono_update()
        self.sprites.update()
        
        # dessin
        self.surface.fill(GRIS)
        self.sprites.draw(self.surface)
        
        self.quadrillage()
        self.display_surface.blit(self.surface, (ECART,ECART))
        pygame.draw.rect(self.display_surface, COULEUR_LIGNE, self.rect, 2, 5)
        

class Bloc(pygame.sprite.Sprite):
    
    def __init__(self,groupe, position, couleur):
        #general
        super().__init__(groupe)
        self.image = pygame.Surface((TAILLE_CARREAU,TAILLE_CARREAU))
        self.image.fill(couleur)
        
        #position
        self.position = pygame.Vector2(position) + COMPENSATION_BLOC
        self.rect = self.image.get_rect(topleft = self.position * TAILLE_CARREAU)
        
    def update(self):
        self.rect.topleft = self.position * TAILLE_CARREAU
        
class Tetromino:
    def __init__(self, forme, groupe):
        
        #setup
        self.bloc_positions = TETROMINOS[forme]["forme"]
        self.couleur = TETROMINOS[forme]["couleur"]
        
        #création de blocs
        self.blocs = [Bloc(groupe,position,self.couleur) for position in self.bloc_positions]
    
    def bouger_horizontal (self,nombre):
        for bloc in self.blocs:
            bloc.position.x += nombre
    
    def bouger_bas(self):
        for bloc in self.blocs:
            bloc.position.y += 1
    
    def bouger_haut(self):
        for bloc in self.blocs:
            bloc.position.y -= 1
        
import pygame 

# Taille Jeu 
COLONNES = 10
RANGEES = 20
TAILLE_CARREAU = 32.5
LARGEUR_JEU, HAUTEUR_JEU = COLONNES * TAILLE_CARREAU, RANGEES * TAILLE_CARREAU

# Taille ecran a droite
ECRAN_LARGEUR = 200
TAILLE_PROCHAINE_PIECE = 0.7
SCORE_HAUTEUR = 1 - TAILLE_PROCHAINE_PIECE

# fenetre
ECART = 20
FENETRE_LARGEUR = LARGEUR_JEU + ECRAN_LARGEUR + ECART * 3
FENETRE_HAUTEUR = HAUTEUR_JEU + ECART * 2

# parametres generaux 
VITESSE_MAJ_JEU = 600
TEMPS_ATTENTE_DEPLACEMENT = 200
TEMPS_ATTENTE_ROTA = 200
COMPENSATION_BLOC = pygame.Vector2(COLONNES // 2, -1)

# COULEURS 
JAUNE = '#f1e60d'
ROUGE = '#e51b20'
BLEU = '#204b9b'
VERT = '#65b32e'
VIOLET = '#7b217f'
CYAN = '#6cc6d9'
ORANGE = '#f07e13'
GRIS = '#1C1C1C'
COULEUR_LIGNE = '#FFFFFF'

# formes
TETROMINOS = {
    'T': {'forme': [(0,0), (-1,0), (1,0), (0,-1)], 'couleur': VIOLET},
    'O': {'forme': [(0,0), (0,-1), (1,0), (1,-1)], 'couleur': JAUNE},
    'J': {'forme': [(0,0), (0,-1), (0,1), (-1,1)], 'couleur': BLEU},
    'L': {'forme': [(0,0), (0,-1), (0,1), (1,1)], 'couleur': ORANGE},
    'I': {'forme': [(0,0), (0,-1), (0,-2), (0,1)], 'couleur': CYAN},
    'S': {'forme': [(0,0), (-1,0), (0,-1), (1,-1)], 'couleur': VERT},
    'Z': {'forme': [(0,0), (1,0), (0,-1), (-1,-1)], 'couleur': ROUGE},
    #'-': {'forme': [(1,0),(3,0)], 'couleur': ROUGE}
}

SCORE_DATA = {1: 40, 2: 100, 3: 300, 4: 1200}

